import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { auth } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  //funtions for DB
  books:any;
  book:any;

  getBooks():Observable<any[]>{
    return this.db.collection('books').valueChanges({idField:'id'});
  }

  getBook(id:string):Observable<any>{
    return this.db.doc(`books/${id}`).get()
  }
  

  addbook(title:string,author:string){
    const book = {title:title, author:author}
    this.db.collection('books').add(book);  
  }


  deletebook(id:string){
    this.db.doc(`books/${id}`).delete();
  }

  updatebook(id:string,title:string,author:string){
    //const book = {title:title, author:author}
    this.db.doc(`books/${id}`).update({title:title,author:author});
  }
  
  constructor(private db:AngularFirestore){}

}

/*functions for local
  books:any= [{id:1,title:'Alice in Wonderlandeddd', author:'Lewis Carrol'},{id:2,title:'War and Peace', author:'Leo Tolstoy'}, {id:3,title:'The Magic Mountain', author:'Thomas Mann'}, {id:4,title:'Michal', author:'Amos Os'}];
  book:any;
  /*קריאה של הנתונים מתוך קובץ ג,ייסון בתוך בוק סרביס
  getBooks(){
    const booksObservable = new Observable(
      observer => {
        setInterval(
          () => observer.next(this.books),500
        )
      }
    )
    return booksObservable;
  }


  

  getBook(id:string){
    for (let index = 0; index < this.books.length; index++) {
      console.log(id);
      if(this.books[index].id == id){
      return this.books[index].author;
      }
    }
  }

  editauth(id:string,author:string){
    for (let index = 0; index < this.books.length; index++) {
      console.log(id);
      if(this.books[index].id == id){
      this.books[index].author=author;
      }
    }
  }

  addbook(){
    //setInterval(
      //() => this.books.push({id:'id',title:'100' , author: 'nadav'}) ,500 )
      this.books.push({id:'id',title:'100' , author: 'nadav'})
      }

  constructor(private db:AngularFirestore) { 

  }
}*/
