import { Component, OnInit } from '@angular/core';
import {MatExpansionModule} from '@angular/material/expansion';
import { BooksService } from '../books.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  books:any;
  books$:Observable<any>;



  //title:string='cool';
  //books:object[] = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}, {title:'Michal', author:'Amos Os'}]
  constructor(public booksservice:BooksService ) { }
  
  deletebook(id:string){
    this.booksservice.deletebook(id);
  }

  ngOnInit() {
   // this.books = this.bookservice.getBooks().subscribe(
    //(books) => this.books = books)
    this.books$ = this.booksservice.getBooks();
  }

}
