import { ActivatedRoute, Router } from '@angular/router';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent implements OnInit {
  id:string;
  title:string;
  author:string;
    isEdit:boolean = false;
  buttonText:string = "Add Book"

  constructor(public booksservice:BooksService,private route:ActivatedRoute,private router:Router) { }

  onSubmit(){
    if(this.isEdit){
      this.booksservice.updatebook(this.id, this.title, this.author);
    }
    else{
      this.booksservice.addbook(this.title, this.author)
    }
   // this.booksservice.addBook(this.title, this.author)
    this.router.navigate(['/books']);
  }


  /*for local via service
  onSubmit(){
    this.booksservice.addbook();
    this.router.navigate(['/books'])
  }*/
  ngOnInit() {
    this.id = this.route.snapshot.params.id; 
    console.log(this.id);
    if(this.id){
      this.isEdit = true;
      this.buttonText = "Update Book";
      this.booksservice.getBook(this.id).subscribe(
        book => {
          this.author = book.data().author;
          this.title = book.data().title;
        })
    }

}

}
