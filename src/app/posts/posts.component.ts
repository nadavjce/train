import { auth } from 'firebase';
import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { PostsService } from '../posts.service';
import { Users } from '../interfaces/users';
import { map, catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  postsdata$: Posts[]=[];
  usersdata$: Users[]=[];
  author:string;
  authorMail:string;
  message:string;

  constructor(private postsservice:PostsService) { }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError("res.error")
  }
  savePosts(){
    for (let index = 0; index < this.postsdata$.length; index++) {
      for (let i = 0; i < this.usersdata$.length; i++) {
        if (this.postsdata$[index].userId=this.usersdata$[i].id) {
          this.postsservice.addPost(this.postsdata$[index].title,this.usersdata$[i].name,this.postsdata$[index].body)
        } 
      }
    }
    this.message="Uploaded";
  }
  
  ngOnInit() {
    this.postsservice.getPosts().subscribe(data =>this.postsdata$ = data);
    this.postsservice.getUsers().subscribe(data =>this.usersdata$ = data);
  }
  
}