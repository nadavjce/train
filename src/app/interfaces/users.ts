export interface Users {
    id: string;
    name: string;
    username: String;
    email: String;
}
