import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Posts } from './interfaces/posts';
import { Users } from './interfaces/users';
import { AngularFirestore } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
 // private URL="https://jsonplaceholder.typicode.com/posts/?userId=";
//userId = '1';
//id = '1'
private POSTURL="https://jsonplaceholder.typicode.com/posts"
private USERURL="https://jsonplaceholder.typicode.com/users/"


  constructor(private http:HttpClient,private db:AngularFirestore) { }

    getPosts():Observable<Posts[]>{
   // return this.http.get<Posts[]>(`${this.URL}${this.userId}&id=${this.id}`);
   return this.http.get<Posts[]>(`${this.POSTURL}`);
   }

   getUsers():Observable<Users[]>{
    return this.http.get<Users[]>(`${this.USERURL}`);
    }

    addPost(title:string,author:string,body:string){
      const post = {title:title, author:author,body:body}
      this.db.collection('posts').add(post); 
    }
    
}